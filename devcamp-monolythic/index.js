const express = require("express"); // Tương tự : import express from "express";
const path = require("path");

// Khởi tạo Express App
const app = express();

const port = 8000;

app.get("/", (request, response) => {
    console.log(__dirname);
     //response.send( "<h1>Hello world<h1>")
    response.sendFile(path.join(__dirname + "/views/pizza365index.html"))
})

app.get("/about", (request, response) => {
    console.log(__dirname);

    response.sendFile(path.join(__dirname + "/views/about.html"))
})

app.get("/sitemap", (request, response) => {
    console.log(__dirname);

    response.sendFile(path.join(__dirname + "/views/sitemap.html"))
})
app.use(express.static(__dirname + "/views"))

app.listen(port, () => {
    console.log(`App Listening on port ${port}`);
})